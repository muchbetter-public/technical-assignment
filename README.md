# MuchBetter Technical Assignment

## Submission

Please upload your completed assignment to either GitHub or GitLab and share the repository link with us through email. Please include the answers to the questions in the email. Do not squash your commits; it is nice to see how the project develops. Enjoy!

## Task: MuchBetter Mini Wallet

The task involves creating a service that represents a simplified version of the MuchBetter e-wallet app. We anticipate that it will take approximately a week to complete the assignment.

You are welcome to use any additional dependencies or libraries that you prefer. The assignment should include unit and integration tests; it is not necessary to have a full test suite, but we do expect a few solid examples. Although dockerizing the service is optional, it is strongly encouraged.

### Technology Stack

- Language: Java
- Framework: Spring WebFlux (Important: Reactive, not imperative, programming)
- In-Memory datastore: Redis (Note: All persistence operations to be performed against the in-memory datastore)

## API

The service needs four REST endpoints.

### Login

Accepts no input and returns a token. The token will need to be supplied in the Authorization header in subsequent requests. Every call returns a new token and creates a new user, with a preset balance in a preset currency.

```
POST /login
```

### Spend

Accepts an Authorization header and a JSON body representing a single transaction.

```
POST /spend

{
    "date": "2022-01-01",
    "description": "Coffee",
    "amount": 10,
    "currency": "EUR"
}
```

### Balance

Accepts an Authorization header and returns the current balance along with the currency code.

```
GET /balance

{
    "amount": 100,
    "currency": "EUR"
}
```

### Transactions

Accepts an Authorization header and returns a list of transactions.

```
GET /transactions

[
    {
        "date": "2022-01-01",
        "description": "Coffee",
        "amount": 10,
        "currency": "EUR"
    }
]
```

## Questions

Please answer the following questions and provide the answers in the email containing the details of your submission:

1.	What would you add to your solution if you spent more time on the coding test?
2.	How would you improve the APIs that you just used?
3.	What is your favorite framework/library/package that you love but couldn't use in the task? What do you like about it so much?